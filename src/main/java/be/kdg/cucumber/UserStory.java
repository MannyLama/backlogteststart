package be.kdg.cucumber;

public class UserStory {
    private int id;
    private String omschrijving;
    private String toevoeging;
    private int prioriteit;

    public UserStory(int id, String omschrijving, String toevoeging, int prioriteit) {
        this.id = id;
        this.omschrijving = omschrijving;
        this.toevoeging = toevoeging;
        this.prioriteit = prioriteit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    public int getPrioriteit() {
        return prioriteit;
    }

    public void setPrioriteit(int prioriteit) {
        this.prioriteit = prioriteit;
    }

    @Override
    public String toString() {
        return "UserStory{" +
                "id=" + id +
                ", omschrijving='" + omschrijving + '\'' +
                ", toevoeging='" + toevoeging + '\'' +
                ", prioriteit=" + prioriteit +
                '}';
    }
}
