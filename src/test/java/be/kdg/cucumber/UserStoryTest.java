package be.kdg.cucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

public class UserStoryTest {
    private final Backlog backlog = new Backlog();

    private UserStory u2;

    @Given("I want to add a user story")
    public void iWantToAddAUserStory() {
        u2 = new UserStory(2, "new user story", "blabla", 1);
    }

    @And("I already have {int} user story")
    public void iAlreadyHaveUserStory(int arg0) {
        for (int i = 0; i < arg0; i++) {
            UserStory u1 = new UserStory(i, "user story", "tuuut", 1);
            backlog.addStory(u1);
        }
    }

    @When("I add my user story")
    public void iAddMyUserStory() {
        backlog.addStory(u2);
    }

    @Then("I should have {int} user story")
    public void iShouldHaveUserStory(int arg0) {
        assertEquals(backlog.countStories(), arg0);
    }


    @When("I add my user story with same id")
    public void iAddMyUserStoryWithSameId() {
        backlog.addStory(new UserStory(1, "test", "tuuut", 3));
    }



    @Given("I want to view a user story with id {int}")
    public void iWantToViewAUserStoryWithId(int arg0) {
        backlog.addStory(new UserStory(arg0, "user storyy" , "other string", 2));
    }


    @When("I search for the user story with id {int}")
    public void iSearchForTheUserStoryWithId(int arg0) {
        backlog.getStory(arg0);
    }

    @Then("I should see the same user story with id {int}")
    public void iShouldSeeTheSameUserStoryWithId(int arg0) {
        assertEquals(backlog.getStory(arg0).toString(), new UserStory(arg0, "user storyy" , "other string", 2).toString());
    }
}
