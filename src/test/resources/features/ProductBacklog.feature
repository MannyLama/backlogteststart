Feature: User story

  Background:
  Given: I have 2 user story

  Scenario: Adding user story
    Given I want to add a user story
    And I already have 2 user story
    When I add my user story
    Then I should have 3 user story

  Scenario: Adding user story with an existsing ID
    Given I want to add a user story
    And I already have 2 user story
    When I add my user story with same id
    Then I should have 2 user story

  Scenario: Viewing a user story
    Given I want to view a user story with id 100
    When I search for the user story with id 100
    Then I should see the same user story with id 100